/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package com.booleanbyte.addon.highresexport.module.fileio;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser.ExtensionFilter;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeColormap;
import net.worldsynth.extent.WorldExtent;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.WorldExtentParameter;
import net.worldsynth.standalone.ui.parameters.ExtentParameterDropdownSelector;
import net.worldsynth.standalone.ui.parameters.FileParameterField;
import net.worldsynth.standalone.ui.parameters.IntegerParameterSlider;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleColormapHighresExporter extends AbstractModule {

	private int resolutionMultiplier = 1;
	private WorldExtentParameter exportExtent;
	private File exportFile = new File(System.getProperty("user.home") + "/colormap_export.png");
	
	@Override
	protected void postInit() {
		exportExtent = getNewExtentParameter();
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		return inputs.get("colormap");
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeColormap colormapRequestData = (DatatypeColormap) outputRequest.data;
		
		inputRequests.put("colormap", new ModuleInputRequest(getInput(0), colormapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Colormap heigh-res exporter";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.EXPORTER_COLORMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeColormap(), "Colormap")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeColormap(), "Troughput", false)
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		ExtentParameterDropdownSelector parameterExtent = exportExtent.getDropdownSelector("Export extent");
		FileParameterField parameterFile = new FileParameterField("Save file", exportFile, true, false, new ExtensionFilter("PNG", "*.png"));
		IntegerParameterSlider parameterResolutionMultiplier = new IntegerParameterSlider("ResolutionMultiplier", 1, 8, resolutionMultiplier);
		
		Button exportButton = new Button("Export colormap");
		exportButton.setOnAction(e -> {
			if(parameterExtent.getValue() == null) {
				return;
			}
			
			float[][][] colormap = getColormap(parameterExtent.getValue(), parameterResolutionMultiplier.getValue());
			writeImageToFile(colormap, parameterFile.getValue());
		});
		
		parameterExtent.addToGrid(pane, 0);
		parameterResolutionMultiplier.addToGrid(pane, 1);
		parameterFile.addToGrid(pane, 2);
		pane.add(exportButton, 1, 3);
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			exportExtent.setExtent(parameterExtent.getValue());
			exportFile = parameterFile.getValue();
			resolutionMultiplier = parameterResolutionMultiplier.getValue();
		};
		
		return applyHandler;
	}
	
	private float[][][] getColormap(WorldExtent extent, int resolutionMultiplier) {
		ModuleInputRequest request = new ModuleInputRequest(getInput(0), new DatatypeColormap(extent.getX(), extent.getZ(), extent.getWidth(), extent.getLength(), 1.0 / (double) resolutionMultiplier));
		DatatypeColormap cmd = (DatatypeColormap) buildInputData(request);
		return cmd.colorMap;
	}
	
	private void writeImageToFile(float[][][] colormap, File f) {
		try {
			BufferedImage image = new BufferedImage(colormap.length, colormap[0].length, BufferedImage.TYPE_INT_RGB);
			for(int x = 0; x < colormap.length; x++) {
				System.out.println("Converting to buffer: " + (int)(100.0f*(float)x/(float)colormap.length) + "%");
				for(int y = 0; y < colormap[0].length; y++) {
					Color c = new Color(colormap[x][y][0], colormap[x][y][1], colormap[x][y][2]);
					image.setRGB(x, y, c.getRGB());
				}
			}
			ImageIO.write(image, "png", f);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("extent", exportExtent.getExtentAsString()));
		paramenterElements.add(new Element("file", exportFile.getPath()));
		paramenterElements.add(new Element("resolutionmultiplier", String.valueOf(resolutionMultiplier)));
		
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("extent")) {
				exportExtent.setExtentAsString(e.content);
			}
			else if(e.tag.equals("file")) {
				exportFile = new File(e.content);
			}
			else if(e.tag.equals("resolutionmultiplier")) {
				resolutionMultiplier = Integer.parseInt(e.content);
			}
		}
	}
}
