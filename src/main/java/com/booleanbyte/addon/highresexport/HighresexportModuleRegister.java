/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package com.booleanbyte.addon.highresexport;

import com.booleanbyte.addon.highresexport.module.fileio.ModuleColormapHighresExporter;
import com.booleanbyte.addon.highresexport.module.fileio.ModuleHeightmapHeighresExporter;

import net.worldsynth.module.AbstractModuleRegister;
import net.worldsynth.module.ClassNotModuleExeption;

public class HighresexportModuleRegister extends AbstractModuleRegister {
	
	public HighresexportModuleRegister() {
		super();
		
		try {
			registerModule(ModuleHeightmapHeighresExporter.class, "\\Highres export");
			registerModule(ModuleColormapHighresExporter.class, "\\Highres export");
		} catch (ClassNotModuleExeption e) {
			throw new RuntimeException(e);
		}
	}
}
