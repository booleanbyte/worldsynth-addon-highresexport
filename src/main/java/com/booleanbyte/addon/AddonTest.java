/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package com.booleanbyte.addon;

import com.booleanbyte.addon.highresexport.HighresexportModuleRegister;

import net.worldsynth.patcher.WorldSynthPatcher;

public class AddonTest {
	
	public static void main(String[] args) {
		WorldSynthPatcher.startPatcher(args, new HighresexportModuleRegister());
	}
}
